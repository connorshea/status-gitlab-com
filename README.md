This is the Ruby on Rails application that powers [status.gitlab.com](https://status.gitlab.com/).

This software is MIT Expat licensed.
