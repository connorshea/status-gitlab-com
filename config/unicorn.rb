# Minimal sample configuration file for Unicorn (not Rack) when used
# with daemonization (unicorn -D) started in your working directory.
#
# See http://unicorn.bogomips.org/Unicorn/Configurator.html for complete
# documentation.
# See also http://unicorn.bogomips.org/examples/unicorn.conf.rb for
# a more verbose configuration using more features.

listen '/home/deploy/status-gitlab-com/shared/unicorn.sock'
worker_processes 2 # this should be >= nr_cpus
pid '/home/deploy/status-gitlab-com/shared/unicorn.pid'
stderr_path '/home/deploy/status-gitlab-com/log/unicorn.log'
stdout_path '/home/deploy/status-gitlab-com/log/unicorn.log'
