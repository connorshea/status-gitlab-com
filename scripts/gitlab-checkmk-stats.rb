#!/usr/bin/env ruby
require 'socket'
require 'json'

socket_file = '/omd/sites/gitlab/tmp/run/live'
stats_file = '/tmp/gitlab-checkmk-stats'

def get_value(text, filter)
  value = text.gsub(filter, '')
  value.gsub!(/;.*/, '')
end

def set_value(hash, key, value)
  hash[key] = [] unless hash.has_key?(key)

  hash[key].shift if hash[key].count > 1440
  hash[key] = hash[key] + [value]
  return hash
end

socket = UNIXSocket.new(socket_file)
socket.puts("GET services\nFilter: host_name = gitlab.com\nColumns: display_name state perf_data host_display_name host_state host_perf_data\nOutputFormat: json")
socket.shutdown('SHUT_WR')
services = JSON.parse(socket.read)
socket.close

begin
  stats_file = File.new(stats_file, 'r')
  stats = JSON.parse(stats_file.read)
  stats_file.close
rescue
  stats = {}
end

time = Time.new.to_i
services.each do | service |
  if service[0].include?('SSH')
    value = get_value(service[5], 'time=')
    stats = set_value(stats, 'latency', {'time' => time, 'state' => service[4], 'value' => value})
    name = 'ssh response time'
    value = get_value(service[2], 'time=')
    stats = set_value(stats, name, {'time' => time, 'state' => service[1], 'value' => value})
  end
  if service[0] == "HTTP /"
    name = 'http response time'
    value = get_value(service[2], 'time=')
    stats = set_value(stats, name, {'time' => time, 'state' => service[1], 'value' => value})
  end
  if service[0].include?('gitlab-ce')
    name = 'http project response time'
    value = get_value(service[2], 'time=')
    stats = set_value(stats, name, {'time' => time, 'state' => service[1], 'value' => value})
  end
end

json = JSON.generate(stats)
tmp_stats_file = File.new(stats_file, 'w')
tmp_stats_file.write(json)
tmp_stats_file.close
puts json
